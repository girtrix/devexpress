﻿Imports System.Data.Entity
Imports System.Data.Entity.ModelConfiguration.Conventions

Public Class AdvContext
    Inherits DbContext

    Public Sub New()
        MyBase.New("name=AdventureWorks2012")
    End Sub

    Property People As DbSet(Of Person)
    Property Emails As DbSet(Of PersonEmail)
End Class
