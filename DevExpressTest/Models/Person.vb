﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

<Table("Person.Person")>
Public Class Person
    <Key>
    Property BusinessEntityID() As Integer

    Property PersonType() As String

    Property Title() As String

    Property FirstName() As String

    Property LastName() As String

    Property EmailPromotion() As Integer

    Property ModifiedDate() As Date
End Class
