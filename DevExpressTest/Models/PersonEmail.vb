﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

<Table("Person.EmailAddress")>
Public Class PersonEmail
    <Key, Column(Order:=0)>
    Property BusinessEntityID() As Integer

    <Key, Column(Order:=1)>
    Property EmailAddressID() As Integer

    Property EmailAddress() As String
End Class
