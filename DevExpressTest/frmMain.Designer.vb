﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.peopleBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colBusinessEntityID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPersonType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTitle = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFirstName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEmailPromo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colModifiedDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.emailBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.luEmail = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peopleBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.emailBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.luEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.peopleBindingSource
        Me.GridControl1.Location = New System.Drawing.Point(12, 211)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.luEmail})
        Me.GridControl1.Size = New System.Drawing.Size(745, 268)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'peopleBindingSource
        '
        Me.peopleBindingSource.DataSource = GetType(DevExpressTest.Person)
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBusinessEntityID, Me.colPersonType, Me.colTitle, Me.colFirstName, Me.colLastName, Me.colEmailPromo, Me.colModifiedDate})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        '
        'colBusinessEntityID
        '
        Me.colBusinessEntityID.FieldName = "BusinessEntityID"
        Me.colBusinessEntityID.Name = "colBusinessEntityID"
        Me.colBusinessEntityID.Visible = True
        Me.colBusinessEntityID.VisibleIndex = 0
        '
        'colPersonType
        '
        Me.colPersonType.FieldName = "PersonType"
        Me.colPersonType.Name = "colPersonType"
        Me.colPersonType.Visible = True
        Me.colPersonType.VisibleIndex = 1
        '
        'colTitle
        '
        Me.colTitle.FieldName = "Title"
        Me.colTitle.Name = "colTitle"
        Me.colTitle.Visible = True
        Me.colTitle.VisibleIndex = 2
        '
        'colFirstName
        '
        Me.colFirstName.FieldName = "FirstName"
        Me.colFirstName.Name = "colFirstName"
        Me.colFirstName.Visible = True
        Me.colFirstName.VisibleIndex = 3
        '
        'colLastName
        '
        Me.colLastName.FieldName = "LastName"
        Me.colLastName.Name = "colLastName"
        Me.colLastName.Visible = True
        Me.colLastName.VisibleIndex = 4
        '
        'colEmailPromo
        '
        Me.colEmailPromo.Caption = "Email"
        Me.colEmailPromo.ColumnEdit = Me.luEmail
        Me.colEmailPromo.FieldName = "EmailPromotion"
        Me.colEmailPromo.Name = "colEmailPromo"
        Me.colEmailPromo.Visible = True
        Me.colEmailPromo.VisibleIndex = 6
        '
        'colModifiedDate
        '
        Me.colModifiedDate.FieldName = "ModifiedDate"
        Me.colModifiedDate.Name = "colModifiedDate"
        Me.colModifiedDate.Visible = True
        Me.colModifiedDate.VisibleIndex = 5
        '
        'emailBindingSource
        '
        Me.emailBindingSource.DataSource = GetType(DevExpressTest.PersonEmail)
        '
        'luEmail
        '
        Me.luEmail.AutoHeight = False
        Me.luEmail.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.luEmail.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("BusinessEntityID", "Business Entity ID", 90, DevExpress.Utils.FormatType.Numeric, "", True, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EmailAddressID", "Email Address ID", 90, DevExpress.Utils.FormatType.Numeric, "", True, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EmailAddress", "Email Address", 200, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near)})
        Me.luEmail.DataSource = Me.emailBindingSource
        Me.luEmail.DisplayMember = "EmailAddress"
        Me.luEmail.Name = "luEmail"
        Me.luEmail.ValueMember = "EmailAddressID"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(769, 491)
        Me.Controls.Add(Me.GridControl1)
        Me.Name = "frmMain"
        Me.Text = "Main"
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peopleBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.emailBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.luEmail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents peopleBindingSource As BindingSource
    Friend WithEvents colBusinessEntityID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPersonType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTitle As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFirstName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colModifiedDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEmailPromo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents emailBindingSource As BindingSource
    Friend WithEvents luEmail As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
End Class
